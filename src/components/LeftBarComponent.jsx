import React, { Component } from "react";

export class LeftBarComponent extends Component {
  render() {
    return (
      <div className="shadow-xl p-5 w-[5%] bg-[#5c5a5a1a] cursor-pointer">
        <ul>
          <li className="object-cover">
            <img
              src={require("../Images/category_icon.png")}
              className="w-8 h-8 mb-10 mt-10 p-1 "
              alt=""
            />
          </li>
          <li className="object-cover">
            <img
              src={require("../Images/cube.png")}
              className="w-8 h-8 mt-2 p-1 "
              alt=""
            />
          </li>
          <li className="object-cover">
            <img
              src={require("../Images/list.png")}
              className="w-8 h-8 mt-2 p-1  "
              alt=""
            />
          </li>
          <li className="object-cover">
            <img
              src={require("../Images/messenger.png")}
              className="w-8 h-8 mt-2 p-1  "
              alt=""
            />
          </li>
          <li className="object-cover">
            <img
              src={require("../Images/list.png")}
              className="w-8 h-8 mt-2 mb-10 p-1  "
              alt=""
            />
          </li>
          <li className="object-cover">
            <img
              src={require("../Images/success.png")}
              className="w-8 h-8 mt-2 p-1  "
              alt=""
            />
          </li>
          <li className="object-cover">
            <img
              src={require("../Images/security.png")}
              className="w-8 h-8 mt-2 p-1  "
              alt=""
            />
          </li>
          <li className="object-cover">
            <img
              src={require("../Images/users.png")}
              className="w-8 h-8 mt-2 mb-10 p-1  "
              alt=""
            />
          </li>
          <li>
            <img
              src={require("../Images/lachlan.jpg")}
              className="w-8 h-8 mt-4   rounded-full"
              alt=""
            />
          </li>
          <li>
            <img
              src={require("../Images/raamin.jpg")}
              className="w-8 h-8 mt-4  rounded-full"
              alt=""
            />
          </li>
          <li>
            <img
              src={require("../Images/nonamesontheway.jpg")}
              className="w-8 h-8 mt-4  rounded-full"
              alt=""
            />
          </li>
          <li>
            <img
              src={require("../Images/plus.png")}
              className="w-8 h-8 mt-4  rounded-full"
              alt=""
            />
          </li>
        </ul>
      </div>
    );
  }
}

export default LeftBarComponent;
