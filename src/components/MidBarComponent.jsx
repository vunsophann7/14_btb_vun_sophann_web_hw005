
import React, { useState } from "react";

const MidBarComponent = ({ dataCard, setDataCard }) => {
  // for store new data from input group
  const [newDataCards, newSetDataCards] = useState([]);
  // input group
  const onInputHandleGroup = (getValue) => {
    newSetDataCards({
      ...newDataCards,
      [getValue.target.name]: getValue.target.value,
    });
  };
  // for submit button and return value
  const onSubmitHandleButton = (event) => {
    event.preventDefault();
      setDataCard([...dataCard, { id: dataCard.length + 1, ...newDataCards }]);
      document.getElementById("form").reset();
  };

  // for get see more detail
  const [seeDetail, setSeeDetail] = useState([]);

  const onButtonChange = (id) => {
    dataCard.map((itemButton) => {
      if (itemButton.id === id) {
        if (itemButton.status === "beach") {
          itemButton.status = "mountain";
        } else if (itemButton.status === "mountain") {
          itemButton.status = "forest";
        } else {
          itemButton.status = "beach";
        }
      }
    });
    setDataCard([...dataCard]);
  };

  return (
    <div className="w-[70%] bg-white">
      <div className="p-4">
        <div className="flex justify-between">
          <h1 className="text-3xl text-black">Good Evening Team!</h1>
          {/* The button to open modal */}
          <label htmlFor="my-modal-3" className="btn btn-info">
            add new trip
          </label>
        </div>
        {/* <form onSubmit={onSubmitHandleButton}> */}
        <form onSubmit={onSubmitHandleButton} id="form">
          <input type="checkbox" id="my-modal-3" className="modal-toggle" />
          <div className="modal">
            <div className="modal-box relative bg-slate-100 text-black">
              <label
                htmlFor="my-modal-3"
                className="btn btn-error btn-sm btn-circle absolute right-2 top-2"
              >
                ✕
              </label>
              <div className="form-contro">
                <label className="label">
                  <span className="label-text text-black">Title</span>
                </label>
                <input
                  type="text"
                  placeholder="Title"
                  name="title"
                  className="input input-bordered input-info w-full  bg-white"
                  onChange={onInputHandleGroup}
                />
                <label className="label">
                  <span className="label-text text-black">Description</span>
                </label>
                <input
                  type="text"
                  name="description"
                  placeholder="Description"
                  className="input input-bordered input-info w-full  bg-white"
                  onChange={onInputHandleGroup}
                />
                <label className="label">
                  <span className="label-text text-black">People going</span>
                </label>
                <input
                  type="text"
                  name="peopleGoing"
                  placeholder="People going"
                  className="input input-bordered input-info w-full  bg-white"
                  onChange={onInputHandleGroup}
                />
                <label className="label">
                  <span className="label-text text-black">
                    Type of Adventure
                  </span>
                </label>
                <select
                  className="select select-primary bg-white w-full mt-4"
                  name="status"
                  onChange={onInputHandleGroup}
                >
                  <option disabled selected>
                    --- Choose any option ---
                  </option>
                  {dataCard.map((itemCard) => (
                    <option>{itemCard.status}</option>
                  ))}
                </select>
                <button className="btn w-32 rounded mt-4 text-white bg-[#2E4251]">
                  submit
                </button>
              </div>
            </div>
          </div>
        </form>
        {/* card */}
        <div>
          <div className="card text-neutral-content grid grid-cols-3 gap-2 mt-4 p-0">
            {dataCard.map((itemCard) => (
              <div
                key={itemCard.id}
                className="card-body text-white bg-[#2E4251] rounded-xl"
              >
                <h2 className="card-title uppercase">{itemCard.title}</h2>
                <p className="line-clamp-3">{itemCard.description}</p>
                <p className="mt-2 text-stone-300">People Going</p>
                <h2 className="text-2xl">{itemCard.peopleGoing}</h2>
                <div className="card-actions justify-between">
                  {/* <button className="btn btn-info w-32"> */}
                  <button
                    className={`${
                      itemCard.status == "beach"
                        ? "btn btn-info w-32"
                        : itemCard.status == "mountain"
                        ? "btn btn-success w-32"
                        : "btn btn-warning w-32"
                    }`}
                    onClick={() => onButtonChange(itemCard.id)}
                  >
                    {itemCard.status}
                  </button>
                  <label
                    onClick={() => {
                      setSeeDetail(itemCard);
                    }}
                    htmlFor="my-modal-4"
                    className="btn w-32"
                  >
                    See Detail
                  </label>
                  <input
                    type="checkbox"
                    id="my-modal-4"
                    className="modal-toggle"
                  />
                  <div className="modal">
                    <div className="modal-box relative bg-white text-black">
                      <label
                        htmlFor="my-modal-4"
                        className="btn btn-sm btn-circle absolute right-2 top-2"
                      >
                        ✕
                      </label>
                      <h2 className="text-lg font-bold uppercase">
                        {seeDetail.title}
                      </h2>
                      <p className="py-4">{seeDetail.description}</p>
                      <p className="mt-2 text-black">People Going</p>
                      <h2 className="text-2xl">
                        Around
                        <span className="bg-[#FFEAD0]">
                          {seeDetail.peopleGoing}
                        </span>
                        people going there
                      </h2>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default MidBarComponent;
