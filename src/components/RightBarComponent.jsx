import React, { Component } from "react";

export class RightBarComponent extends Component {
  render() {
    return (
      <div className="w-[25%] bg-[#F8F9FC] object-cover h-screen text-white relative right-0 top-0">
        {/* full bg img right bar */}
        <img
          src={require("../Images/camping.jpg")}
          className="w-full h-full"
          alt=""
        />
        <div className="flex justify-end absolute right-0 top-0 p-4">
          <ul className="flex justify-between cursor-pointer">
            <li>
              <img
                src={require("../Images/notification.png")}
                className="w-6 h-6 rounded-full mt-2 mr-2 p-1 bg-white"
                alt=""
              />
            </li>
            <li>
              <img
                src={require("../Images/messenger.png")}
                className="w-6 h-6 rounded-full mt-2 mr-2 p-1 bg-white"
                alt=""
              />
            </li>
            <li>
              <img
                src={require("../Images/lachlan.jpg")}
                className="w-10 h-10 rounded-full"
                alt=""
              />
            </li>
          </ul>
        </div>
        <div className="mt-10 flex justify-end absolute right-0 top-8 p-4">
          <button className="p-3 rounded-lg w-52 mb-5 text-black bg-[#FFEAD0]">
            My Amazing Trip
          </button>
        </div>
        <div className="absolute right-0 top-48 p-4 ">
          <p className="text-2xl">
            I Like laying down on the sand and looking at the mooon.
          </p>
        </div>
        <div className="mt-10 absolute right-0 top-72 p-4">
          <p className="mb-3">27 people going to this trip</p>
          <div>
            <ul className="flex justify-between cursor-pointer">
              <li>
                <img
                  src={require("../Images/lachlan.jpg")}
                  className="w-10 h-10 rounded-full mr-8"
                  alt=""
                />
              </li>
              <li>
                <img
                  src={require("../Images/raamin.jpg")}
                  className="w-10 h-10 rounded-full mr-8"
                  alt=""
                />
              </li>
              <li>
                <img
                  src={require("../Images/christina.jpg")}
                  className="w-10 h-10 rounded-full mr-8"
                  alt=""
                />
              </li>
              <li>
                <img
                  src={require("../Images/nonamesontheway.jpg")}
                  className="w-10 h-10 rounded-full mr-8"
                  alt=""
                />
              </li>
              <li className="w-10 h-10 flex justify-center items-center rounded-full mr-3 text-black bg-[#FFEAD0]">
                <span>23+</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default RightBarComponent;
